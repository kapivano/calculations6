﻿using System;

namespace Calculations
{
    public static class Calculator
    {
        public static double GetSumOne(int n)
        {
            double res = 0;
            for (int i = 1; i <= n; i++)
            {
                res += 1d / i;
            }

            return res;
        }

        public static double GetSumTwo(int n)
        {
            double a = 1;
            double res = 0;
            for (double i = 1; i <= n; i++)
            {
                res += a / (i * (i + 1));
                a *= -1;
            }

            return res;
        }

        public static double GetSumThree(int n)
        {
            double res = 0;
            for (double i = 1; i <= n; i++)
            {
                res += 1d / (i * i * i * i * i);
            }

            return res;
        }

        public static double GetSumFour(int n)
        {
            double res = 0;
            for (double i = 1; i <= n; i++)
            {
                res += 1d / (((2 * i) + 1) * ((2 * i) + 1));
            }

            return res;
        }

        public static double GetProductOne(int n)
        {
            double res = 1;
            for (double i = 1; i <= n; i++)
            {
                res *= 1d + (1d / (i * i));
            }

            return res;
        }

        public static double GetSumFive(int n)
        {
            double a = -1;
            double res = 0;
            for (double i = 1; i <= n; i++)
            {
                res += a / ((2d * i) + 1d);
                a *= -1d;
            }

            return res;
        }

        public static double GetSumSix(int n)
        {
            double res = 0;
            double a = 0;
            double b = 1;
            for (double i = 1; i <= n; i++)
            {
                a += 1d / i;
                b *= i;
                res += b / a;
            }

            return res;
        }

        public static double GetSumSeven(int n)
        {
            double res = 0;
            for (int i = 1; i <= n; i++)
            {
                res = 2 + Math.Sqrt(res);
            }

            return Math.Sqrt(res);
        }

        public static double GetSumEight(int n)
        {
            double res = 0;
            double a = 0;
            double rad;
            for (double i = 1; i <= n; i++)
            {
                rad = (Math.PI * i) / 180;
                a += Math.Sin(rad);
                res += 1d / a;
            }

            return res;
        }
    }
}
